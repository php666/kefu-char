<?php 
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */
use \Workerman\Worker;
use \GatewayWorker\Gateway;
use \Workerman\Autoloader;

// 自动加载类
require_once '../vendor/autoload.php';
require_once '../extend/MySqli.class.php'; //加载mysqli基本的操作类，封装了一些常用的操作数据库的方法
$conf = include('../extend/mysql.conf.php');
$mysql = new mysql();
$mysql->connect($conf);
$sql = "SELECT * FROM ".$conf['prefix']."plugin_lyz_kefu_chat_option_win";
$list = $mysql->getRow($sql);
$list = json_decode($list['option'],true);
$list = $list['option'];

// gateway 进程
$gateway = new Gateway($list['gateway']);
// 设置名称，方便status时查看
$gateway->name = $list['gateway_processes_name'];
// 设置进程数，gateway进程数建议与cpu核数相同
$gateway->count = $list['gateway_processes_num'];
// 分布式部署时请设置成内网ip（非127.0.0.1）
$gateway->lanIp = $list['inner_ip'];
// 内部通讯起始端口，假如$gateway->count=4，起始端口为4000
// 则一般会使用4000 4001 4002 4003 4个端口作为内部通讯端口 
$gateway->startPort = $list['gateway_start_port'];
// 心跳间隔
$gateway->pingInterval = $list['pingInterval'];
// 心跳数据
$gateway->pingData = $list['pingdata'];
// 服务注册地址
$gateway->registerAddress = $list['register_ip'];

// 如果不是在根目录启动，则运行runAll方法
if(!defined('GLOBAL_START'))
{
    define('GLOBAL_START',1);
    Worker::runAll();
}

