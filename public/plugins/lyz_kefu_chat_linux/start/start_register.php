<?php
use \Workerman\Worker;
use \GatewayWorker\Register;
use think\config;

// 自动加载类
require_once '../vendor/autoload.php';
require_once '../extend/MySqli.class.php'; //加载mysqli基本的操作类，封装了一些常用的操作数据库的方法
$conf = include('../extend/mysql.conf.php');
$mysql = new mysql();
$mysql->connect($conf);
$sql = "SELECT * FROM ".$conf['prefix']."plugin_lyz_kefu_chat_option_win";
$list = $mysql->getRow($sql);
$list = json_decode($list['option'],true);
$list = $list['option'];


// register 服务必须是text协议
$register = new Register($list['register']);

// 如果不是在根目录启动，则运行runAll方法
if(!defined('GLOBAL_START'))
{
    define('GLOBAL_START',1);
    Worker::runAll();
}

