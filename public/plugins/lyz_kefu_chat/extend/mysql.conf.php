<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/10/21
 * Time: 15:16
 */
//数据库配置文件
$conf = include('../../../../data/conf/database.php');  //获取thinkcmf的数据库信息
return [
    'dbHost' => $conf['hostname'],
    'dbUser' => $conf['username'],
    'dbPasswd' => $conf['password'],
    'dbName' => $conf['database'],
    'dbCharset' => $conf['charset'],
    'prefix' => $conf['prefix'],
    'hostport' => $conf['hostport'],
];