<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace plugins\lyz_kefu_chat\controller;
use cmf\controller\PluginBaseController;
//use plugins\lyz_kefu_char\Model\PluginDemoModel;
use think\Db;
use think\Config;
use think\Session;

class IndexController extends PluginBaseController
{
    function index()
    {
        $users = Db::name("user")->limit(0, 5)->select();
        $this->assign("users", $users);

        return $this->fetch("/index");
    }

    function chat()
    {
        $root = cmf_get_root();
        $pluginRoot = $root . "/plugins/lyz_kefu_chat";
        $public = $pluginRoot . '/view/public/';
        $this->assign('public',$public);  //静态资源路径

        $this->assign('kefu_name',Session::get('kefu_name'));
        $this->assign('kefu_nickname',Session::get('kefu_nickname'));
        $this->assign('kefu_pwd',Session::get('kefu_pwd'));
        //查出状态正常的客服信息
        $user_list = Db::name('plugin_lyz_kefu_chat_user_win')->where('user_status',1)->select();
        $this->assign('user_list',$user_list);
        return $this->fetch("/chat");
    }

    //检测客服的密码是否正确
    function checkPWD()
    {
        if($this->request->isPost()){
            $fl = Db::name('plugin_lyz_kefu_chat_user_win')->where('user_name',$_POST['user_name'])->find();
            if(!empty($fl)){
                if($fl['user_pwd'] == $_POST['user_pwd']){
                    return true;
                }else{
                    return false;
                }
            }
        }
    }

    //客服登录页面
    function kefuLogin()
    {
        if($this->request->isPost()){
            $user_name = $_POST['user_name'];
            $fl = Db::name('plugin_lyz_kefu_chat_user_win')->where('user_name',$user_name)->find();
            if(!empty($fl)){
                if($fl['user_pwd'] == $_POST['user_pwd']){
                    Session::set('kefu_id',$fl['user_id']);
                    Session::set('kefu_name',$fl['user_name']);
                    Session::set('kefu_nickname',$fl['user_nickname']);
                    Session::set('kefu_pwd',$fl['user_pwd']);
                    $this->success("登录成功", cmf_plugin_url('LyzKefuChat://Index/kefuIndex'));
                }else{
                    $this->error("登录失败", cmf_plugin_url('LyzKefuChat://Index/kefuLogin'));
                }
            }else{
                $this->error("登录失败", cmf_plugin_url('LyzKefuChat://Index/kefuLogin'));
            }
        }else{
            return $this->fetch('/kefu_login');
        }
    }

    //客服个人管理页面
    function kefuIndex()
    {
        $kefu_id = Session::get('kefu_id');
        if(!empty($kefu_id)){
            //查出该客服要服务的用户信息
            $list = Db::name('plugin_lyz_kefu_chat_recover_win')->where('kefu_id',$kefu_id)->where('status',1)->select();
            $this->assign('list',$list);
        }else{
            $this->success("请先登录", cmf_plugin_url('LyzKefuChat://Index/kefuLogin'));
        }
        return $this->fetch('/kefu_index');
    }

    //客服注销
    function kefuLogout()
    {
        Session::delete('kefu_id');
        Session::delete('kefu_name');
        Session::delete('kefu_nickname');
        Session::delete('kefu_pwd');
        $this->success("退出成功", cmf_plugin_url('LyzKefuChat://Index/kefuLogin'));
    }
}
