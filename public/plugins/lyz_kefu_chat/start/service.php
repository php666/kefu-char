<?php
/**
 * Created by PhpStorm.
 * User: liyuzhao
 * Date: 2017/10/21
 * Time: 0:10
 */
namespace plugins\lyz_kefu_chat\start;

use think\Db;
use \Workerman\Worker;
use \GatewayWorker\Register;
use \GatewayWorker\Gateway;
use \GatewayWorker\BusinessWorker;
use \Workerman\WebServer;
use \Workerman\Autoloader;

// 自动加载类
require_once '../vendor/autoload.php';
require '.\event\MyEvent.php';
class Service
{
    public function __construct()
    {
        // register 服务必须是text协议
        $register = new Register('text://0.0.0.0:1236');

        // gateway 进程
        $gateway = new Gateway("Websocket://0.0.0.0:7272");
        // 设置名称，方便status时查看
        $gateway->name = 'ChatGateway';
        // 设置进程数，gateway进程数建议与cpu核数相同
        $gateway->count = 4;
        // 分布式部署时请设置成内网ip（非127.0.0.1）
        $gateway->lanIp = '127.0.0.1';
        // 内部通讯起始端口，假如$gateway->count=4，起始端口为4000
        // 则一般会使用4000 4001 4002 4003 4个端口作为内部通讯端口
        $gateway->startPort = 2300;
        // 心跳间隔
        $gateway->pingInterval = 10;
        // 心跳数据
        $gateway->pingData = '{"type":"ping"}';
        // 服务注册地址
        $gateway->registerAddress = '127.0.0.1:1236';

        // bussinessWorker 进程
        $worker = new BusinessWorker();
        // worker名称
        $worker->name = 'ChatBusinessWorker';
        // bussinessWorker进程数量
        $worker->count = 4;
        // 服务注册地址
        $worker->registerAddress = '127.0.0.1:1236';
        /*
         * 设置处理业务的类为MyEvent。
         * 如果类带有命名空间，则需要把命名空间加上，
         * 类似$worker->eventHandler='\my\namespace\MyEvent';
         */
        $worker->eventHandler = 'MyEvent';

        // WebServer
        $web = new WebServer("http://0.0.0.0:55151");
        // WebServer数量
        $web->count = 2;
        // 设置站点根目录
        $web->addRoot('http://thinkcmf.com', '');

        Worker::runAll();
    }
}
new Service();