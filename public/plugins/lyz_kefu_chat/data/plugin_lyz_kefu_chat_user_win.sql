--
-- 表的结构 `cmf_plugin_lyz_kefu_chat_user_win`
--
CREATE TABLE `cmf_plugin_lyz_kefu_chat_user_win` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(150) COMMENT '客户端id',
  `user_name` varchar(100) NOT NULL COMMENT '客服登录账号',
  `user_nickname` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `user_pwd` varchar(100) COMMENT '客服登录密码',
  `user_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '客服状态（1：正常 -1：禁止）',
  `login_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '客服在线状态（1：在线 -1：不在线）',
  `user_num` int(10) unsigned COMMENT '接待人数',
  `create_time` int(10) unsigned COMMENT '客服号创建时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='客服表';

